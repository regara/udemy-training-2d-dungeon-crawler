﻿using System.Collections;
using Assets.Scripts.Helpers;
using UnityEditor;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float playerSpeed;

    private LayerMask _obstacleMask;
    private Vector2 _targetPosition;
    private Transform _GFX;
    private float _flipx;
    private bool _isMoving;

    void Start()
    {
        _obstacleMask = LayerMask.GetMask("Wall", "Enemy");
        _GFX = GetComponentInChildren<SpriteRenderer>().transform;
        _flipx = _GFX.localScale.x;
    }

    void Update()
    {
        Move();
    }

    void Move()
    {
        float horizontal = System.Math.Sign(Input.GetAxisRaw("Horizontal"));
        float vertical = System.Math.Sign(Input.GetAxisRaw("Vertical"));

        if (Mathf.Abs(horizontal) > 0 || Mathf.Abs(vertical) > 0)
        {
            if (Mathf.Abs(horizontal) > 0)
            {
                _GFX.localScale = new Vector2(_flipx * horizontal, _GFX.localScale.y);
            }

            if (!_isMoving)
            {
                if (Mathf.Abs(horizontal) > 0)
                {
                    _targetPosition = new Vector2(transform.position.x + horizontal, transform.position.y);
                }
                else if (Mathf.Abs(vertical) > 0)
                {
                    _targetPosition = new Vector2(transform.position.x, transform.position.y + vertical);
                }

                Collider2D hit = Physics2D.OverlapBox(_targetPosition, Variables.HitSize, 0, _obstacleMask);

                if (!hit)
                {
                    StartCoroutine(SmoothMove());
                }
            }
        }

        IEnumerator SmoothMove()
        {
            _isMoving = true;

            while (Vector2.Distance(transform.position, _targetPosition) > 0.01f)
            {
                transform.position = Vector2.MoveTowards(transform.position, _targetPosition, playerSpeed.DeltaTime());
                yield return null;
            }

            transform.position = _targetPosition;
            _isMoving = false;
        }
    }
}
