﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Helpers;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum DungeonType
{
    Caverns,
    Rooms,
    WindingHalls
};

public class DungeonManager : MonoBehaviour
{
    private readonly Vector2 _hitSize = Variables.HitSize;

    public GameObject[] randomItems, randomEnemies, roundedEdges;
    public GameObject FloorPrefab, WallPrefab, TilePrefab, ExitDoorPrefab;
    [Range(50, 5000)] public int TotalFloorCount;
    [Range(0, 100)] public int itemSpawnRate;
    [Range(0, 100)] public int EnemySpawnRate;
    public bool useRoundedEdges;
    public DungeonType dungeonType;

    [HideInInspector] public float MinX, MaxX, MinY, MaxY;

    private List<Vector3> _floorList = new List<Vector3>();
    private LayerMask _floorMask, _wallMask;

    private DungeonGeneration _dungeonGeneration = new DungeonGeneration();

    void Start()
    {
        _floorMask = LayerMask.GetMask("Floor");
        _wallMask = LayerMask.GetMask("Wall");

        switch (dungeonType)
        {
            case DungeonType.Caverns: RandomCaveWalker(); break;
            case DungeonType.Rooms: RandomRoomWalker(); break;
            case DungeonType.WindingHalls: RandomWindingRoomWalker(); break;
        }
    }

    void Update()
    {
        if (Application.isEditor && Input.GetKeyDown(KeyCode.Backspace))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    void RandomCaveWalker()
    {
        Vector3 currentPosition = Vector3.zero;
        _floorList.Add(currentPosition);

        while (_floorList.Count < TotalFloorCount)
        {
            currentPosition += _dungeonGeneration.RandomDirection(currentPosition);

            if (!_dungeonGeneration.InFloorList(currentPosition, _floorList))
            {
                _floorList.Add(currentPosition);
            }
        }

        StartCoroutine(WaitForFloorsAndWalls());
    }

    void RandomRoomWalker()
    {
        Vector3 currentPosition = Vector3.zero;
        _floorList.Add(currentPosition);

        while (_floorList.Count < TotalFloorCount)
        {
            currentPosition = _dungeonGeneration.RandomCorridor(currentPosition, _floorList);
            _dungeonGeneration.RandomRoom(currentPosition, _floorList);
        }

        StartCoroutine(WaitForFloorsAndWalls());
    }

    void RandomWindingRoomWalker()
    {
        Vector3 currentPosition = Vector3.zero;
        _floorList.Add(currentPosition);

        while (_floorList.Count < TotalFloorCount)
        {
            currentPosition = _dungeonGeneration.RandomCorridor(currentPosition, _floorList);
            int roll = Random.Range(0, 100);
            if (roll > 20)
            {
                _dungeonGeneration.RandomRoom(currentPosition, _floorList);
            }
        }

        StartCoroutine(WaitForFloorsAndWalls());
    }

    IEnumerator WaitForFloorsAndWalls()
    {
        for (int i = 0; i < _floorList.Count; i++)
        {
            GameObject tile = Instantiate(TilePrefab, _floorList[i], Quaternion.identity);
            tile.name = TilePrefab.name;
            tile.transform.SetParent(transform);
        }

        while (FindObjectsOfType<TileSpawner>().Length > 0)
        {
            yield return null;
        }

        ExitDoorway();

        // MinX - 1 = the walls
        // MinX - 2 = outside the walls
        for (var x = (int)MinX - 2; x <= (int)MaxX + 2; x++)
            for (var y = (int)MinY - 2; y <= (int)MaxY + 2; y++)
            {
                Collider2D hitFloor = Physics2D.OverlapBox(new Vector2(x, y), _hitSize, 0, _floorMask);
                if (hitFloor)
                {
                    if (!(Vector2.Equals(hitFloor.transform.position, _floorList[_floorList.Count - 1])))
                    {
                        Collider2D hitTopWall = Physics2D.OverlapBox(new Vector2(x, y + 1), _hitSize, 0, _wallMask);
                        Collider2D hitRightWall = Physics2D.OverlapBox(new Vector2(x + 1, y), _hitSize, 0, _wallMask);
                        Collider2D hitBottomWall = Physics2D.OverlapBox(new Vector2(x, y - 1), _hitSize, 0, _wallMask);
                        Collider2D hitLeftWall = Physics2D.OverlapBox(new Vector2(x - 1, y), _hitSize, 0, _wallMask);

                        RandomItems(hitFloor, hitTopWall, hitRightWall, hitBottomWall, hitLeftWall);
                        RandomEnemies(hitFloor, hitTopWall, hitRightWall, hitBottomWall, hitLeftWall);
                    }
                }

                RoundedEdges(x,y);

            }
    }

    void RoundedEdges(int x, int y)
    {
        if (useRoundedEdges)
        {
            Collider2D hitWall = Physics2D.OverlapBox(new Vector2(x, y), _hitSize, 0, _wallMask);
            
            if (hitWall)
            {
                Collider2D hitTopWall = Physics2D.OverlapBox(new Vector2(x, y + 1), _hitSize, 0, _wallMask);
                Collider2D hitRightWall = Physics2D.OverlapBox(new Vector2(x + 1, y), _hitSize, 0, _wallMask);
                Collider2D hitBottomWall = Physics2D.OverlapBox(new Vector2(x, y - 1), _hitSize, 0, _wallMask);
                Collider2D hitLeftWall = Physics2D.OverlapBox(new Vector2(x - 1, y), _hitSize, 0, _wallMask);

                int wallSidesAsBit = 0;

                if (!hitTopWall) { wallSidesAsBit += 1; }
                if (!hitRightWall) { wallSidesAsBit += 2; }
                if (!hitBottomWall) { wallSidesAsBit += 4; }
                if (!hitLeftWall) { wallSidesAsBit += 8; }

                if (wallSidesAsBit > 0)
                {
                    GameObject edge = Instantiate(roundedEdges[wallSidesAsBit], new Vector2(x, y), Quaternion.identity);
                    edge.name = roundedEdges[wallSidesAsBit].name;
                    edge.transform.SetParent(hitWall.transform);
                }
            }
        }
    }
    


    void RandomEnemies(Collider2D hitFloor, Collider2D hitTopWall, Collider2D hitRightWall, Collider2D hitBottomWall, Collider2D hitLeftWall)
    {
        if (!hitTopWall && !hitRightWall && !hitBottomWall && !hitLeftWall)
        {
            int roll = Random.Range(1, 101);

            if (roll <= EnemySpawnRate)
            {
                int enemyIndex = Random.Range(0, randomEnemies.Length);
                GameObject enemy = Instantiate(randomEnemies[enemyIndex], hitFloor.transform.position, Quaternion.identity);
                enemy.name = randomEnemies[enemyIndex].name;
                enemy.transform.SetParent(hitFloor.transform);
            }
        }
    }

    void RandomItems(Collider2D hitFloor, Collider2D hitTopWall, Collider2D hitRightWall, Collider2D hitBottomWall, Collider2D hitLeftWall)
    {
        if ((hitTopWall || hitRightWall || hitBottomWall || hitLeftWall) && !(hitTopWall && hitBottomWall) && !(hitRightWall && hitLeftWall))
        {
            int roll = Random.Range(1, 101);

            if (roll <= itemSpawnRate)
            {
                int itemIndex = Random.Range(0, randomItems.Length);
                GameObject item = Instantiate(randomItems[itemIndex], hitFloor.transform.position, Quaternion.identity);
                item.name = randomItems[itemIndex].name;
                item.transform.SetParent(hitFloor.transform);
            }
        }
    }

    void ExitDoorway()
    {
        Vector3 doorPosition = _floorList[_floorList.Count - 1];

        GameObject door = Instantiate(ExitDoorPrefab, doorPosition, Quaternion.identity);
        door.name = ExitDoorPrefab.name;
        door.transform.SetParent(transform);
    }
}
