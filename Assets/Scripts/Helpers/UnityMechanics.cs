﻿using UnityEngine;

namespace Assets.Scripts.Helpers
{
    public static class UnityMechanics
    {
        public static float DeltaTime(this float speed)
        {
            return speed * Time.deltaTime;
        }
    }
}
