﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Helpers
{
    public class DungeonGeneration
    {
        public Vector3 RandomDirection(Vector3 currentPosition)
        {
            switch (Random.Range(1, 5))
            {
                case 1:
                    return Vector3.up;
                case 2:
                    return Vector3.right;
                case 3:
                    return Vector3.down;
                case 4:
                    return Vector3.left;
                default: return Vector3.zero;
            }
        }

        public bool InFloorList(Vector3 playerPosition, List<Vector3> floorList)
        {
            for (int i = 0; i < floorList.Count; i++)
            {
                if (Equals(playerPosition, floorList[i]))
                    return true;
            }
            return false;
        }

        public void RandomRoom(Vector3 currentPosition, List<Vector3> floorList)
        {
            // Random Room at the end of the "walk"
            int roomWidth = Random.Range(1, 5);
            int roomHeight = Random.Range(1, 5);

            for (int w = -roomWidth; w <= roomWidth; w++)
            {
                for (int h = -roomHeight; h <= roomHeight; h++)
                {
                    Vector3 offset = new Vector3(w, h, 0);
                    if (!InFloorList(currentPosition + offset, floorList))
                    {
                        floorList.Add(currentPosition + offset);
                    }
                }
            }
        }

        public Vector3 RandomCorridor(Vector3 currentPosition, List<Vector3> floorList)
        {
            Vector3 walkDirection = RandomDirection(currentPosition);
            int walkLength = Random.Range(9, 18);

            for (int i = 0; i < walkLength; i++)
            {
                if (!InFloorList(currentPosition + walkDirection, floorList))
                {
                    floorList.Add(currentPosition + walkDirection);
                }

                currentPosition += walkDirection;
            }

            return currentPosition;
        }
    }
}
