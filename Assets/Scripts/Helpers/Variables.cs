﻿using UnityEngine;

namespace Assets.Scripts.Helpers
{
    public static class Variables
    {
        public static readonly Vector2 HitSize = Vector2.one * 0.8f;
    }
}
