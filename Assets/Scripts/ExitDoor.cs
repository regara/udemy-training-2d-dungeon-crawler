﻿using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class ExitDoor : MonoBehaviour
{
    void Reset()
    {
        GetComponent<Rigidbody2D>().isKinematic = true;
        BoxCollider2D boxCollider = GetComponent<BoxCollider2D>();
        boxCollider.size = Vector2.one * 0.1f;
        boxCollider.isTrigger = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
