﻿using Assets.Scripts.Helpers;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    private DungeonManager _dungeonManager;

    void Awake()
    {
        _dungeonManager = FindObjectOfType<DungeonManager>();
        GameObject floor = Instantiate(_dungeonManager.FloorPrefab, transform.position, Quaternion.identity);
        floor.name = _dungeonManager.FloorPrefab.name;
        floor.transform.SetParent(_dungeonManager.transform);

        if (transform.position.x > _dungeonManager.MaxX)
        {
            _dungeonManager.MaxX = transform.position.x;
        }

        if (transform.position.x < _dungeonManager.MinX)
        {
            _dungeonManager.MinX = transform.position.x;
        }

        if (transform.position.y > _dungeonManager.MaxY)
        {
            _dungeonManager.MaxY = transform.position.y;
        }

        if (transform.position.y < _dungeonManager.MinY)
        {
            _dungeonManager.MinY = transform.position.y;
        }
    }

    void Start()
    {
        LayerMask environmentMask = LayerMask.GetMask("Wall", "Floor");

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                Vector2 targetPos = new Vector2(transform.position.x + x, transform.position.y + y);
                Collider2D hit = Physics2D.OverlapBox(targetPos, Variables.HitSize, 0, environmentMask);

                if (!hit){
                    GameObject wall = Instantiate(_dungeonManager.WallPrefab, targetPos, Quaternion.identity);
                    wall.name = _dungeonManager.WallPrefab.name;
                    wall.transform.SetParent(_dungeonManager.transform);
                }
            }
        }

        Destroy(gameObject);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawCube(transform.position, Vector3.one);
    }
}
